IMAGE_NAME := localhost/autosd-buildbox:latest 

.PHONY: dnf/lock
dnf/lock:
	dnf lockfile --recursive $$(cat rpms.in)

.PHONY: image/build
image/build:
	buildah bud -f Containerfile -t ${IMAGE_NAME} .
